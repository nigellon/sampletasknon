package com.arvoia.sampletask.constants;

public final class AvailResponseConstants {
    public static final String RESP_SUCCESS = "success";
    public static final String RESP_ERROR = "error";
    public static final String RESP_DEFAULT_ERROR_DESCRIP = "vendor service not available";
    public static final int RESP_DEFAULT_ERROR_CODE = 1;

    private AvailResponseConstants(){
        throw new AssertionError();
    }
}
