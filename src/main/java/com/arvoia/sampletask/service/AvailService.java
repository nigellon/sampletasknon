package com.arvoia.sampletask.service;

import com.arvoia.sampletask.model.Error;
import com.arvoia.sampletask.model.Offer;
import com.arvoia.sampletask.model.Vehicle;
import com.arvoia.sampletask.model.Vehicles;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class AvailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AvailService.class);
    @Autowired
    private ObjectMapper objectMapper;

    public Offer getOffer(String vendorResponse) {
        Offer offer = new Offer();
            try {
                List<Vehicle> vehicles = objectMapper.readValue(vendorResponse, Vehicles.class).getVehicleList();
                Comparator<Vehicle> compareByPrice = Comparator.comparing(Vehicle::getBasePrice);
                if(vehicles != null) {
                    Collections.sort(vehicles, compareByPrice);
                    offer.setVehicles(vehicles);
                    offer.setAvailable(vehicles.size());
                }

                offer.setTimestamp(new Date());

            } catch (IOException e) {
                LOGGER.error("Failed to retrieve vehicle availability.", e.getMessage());
            }
        return offer;
        }

    public Error getError(String vendorResponse){
        try {
            return objectMapper.readValue(vendorResponse, Error.class);
        } catch (IOException e) {
            LOGGER.error("Failed to read retrieve vendor availability error.", e.getMessage());
            return new Error();
        }
    }

}

