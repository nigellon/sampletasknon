package com.arvoia.sampletask.model;

import static com.arvoia.sampletask.constants.AvailResponseConstants.RESP_DEFAULT_ERROR_CODE;
import static com.arvoia.sampletask.constants.AvailResponseConstants.RESP_DEFAULT_ERROR_DESCRIP;

public class Error {
    private int code = RESP_DEFAULT_ERROR_CODE; // default
    private String description = RESP_DEFAULT_ERROR_DESCRIP; // default

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Error {" +
                " code=" + code +
                ", description=" + description +
                "}";
    }
}
