package com.arvoia.sampletask.model;

public class Vehicle {

    private String brand;
    private String category;
    private Integer basePrice;

    public String getBrand() {
        return brand;
    }

    public String getCategory() {
        return category;
    }

    public Integer getBasePrice() {
        return basePrice;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setBasePrice(Integer basePrice) {
        this.basePrice = basePrice;
    }

    @Override
    public String toString() {
        return "Vehicle [" +
                    "brand=" + brand +
                    ", category=" + category +
                    ", baseprice=" + basePrice +
                ']';
    }
}

