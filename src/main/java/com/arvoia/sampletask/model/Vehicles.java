package com.arvoia.sampletask.model;

import java.util.List;

public class Vehicles {

    private List<Vehicle> vehicleList;

    public List<Vehicle> getVehicleList() {
        return vehicleList;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicleList = vehicles;
    }
}
