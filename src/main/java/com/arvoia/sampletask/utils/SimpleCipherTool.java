package com.arvoia.sampletask.utils;

public class SimpleCipherTool {

	/**
	 * Supplied cipher key.
	 */
	private char[] key = null;
	/**
	 * Lowercase English alphabet: used for cipher to determine char value via
	 * position. i.e. a = 0, b = 1...z = 25
	 */
	private final String LOWERCASE = "abcdefghijklmnopqrstuvwxyz";

	/**
	 * <p>
	 * Constructor
	 * </p>
	 * 
	 * @param key the cipher keyword supplied by calling client
	 */
	public SimpleCipherTool(String key) {
		if (key == null)
			throw new IllegalArgumentException("Param key: must be String");
	
		this.key = key.toCharArray();
	}

	/**
	 * <p>
	 * Accepts a text phrase to be ciphered. Characters within the phrase are
	 * ciphered by adding the value of the corresponding keyword character to the
	 * corresponding phrase character. If the phrase is longer than the keyword the
	 * process starts from the first keyword character. Uppercase and non alphabetic
	 * characters are ignored and therefore, returned in the cipher.
	 *
	 * </p>
	 * 
	 * @param text incoming phrase to cipher.
	 * @return the resulting cipher
	 */
	public String cipher(String text) {
		if (text == null)
			throw new IllegalArgumentException("Param text param must be String");
		
		char[] charArray = text.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			char ch = charArray[i];
			// apply cipher...
			if (LOWERCASE.indexOf(ch) >= 0) {
				// next char in the key
				char keyChar = key[i % key.length];
				// sum original char value and key char value
				int cipherCharValue = ch + (LOWERCASE.indexOf(keyChar) + 1);
				if (cipherCharValue > 'z') {
					cipherCharValue = (cipherCharValue - LOWERCASE.length());
				}
				charArray[i] = (char) (cipherCharValue);
			}
		}
		return new String(charArray);
	}

	/**
	 * <p>
	 * Accepts a text phrase to be deciphered. Characters within the phrase are
	 * deciphered by subtracting the value of the corresponding keyword character to
	 * the corresponding phrase character. If the phrase is longer than the keyword
	 * the process starts from the first keyword character. Uppercase and non
	 * alphabetic characters are ignored and therefore, returned in the decipher.
	 *
	 * </p>
	 * 
	 * @param cipheredText incoming phrase to cipher.
	 * @return the resulting decipher
	 */
	public String decipher(String cipheredText) {
		if (cipheredText == null)
			throw new IllegalArgumentException("Param cipheredText: must be String");

		char[] charArray = cipheredText.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			char ch = charArray[i];
			// apply decipher...
			if (LOWERCASE.indexOf(ch) >= 0) {
				// next char in the key
				char keyChar = key[i % key.length];
				// Subtract key char value from original char value
				int cipherCharValue = ch - (LOWERCASE.indexOf(keyChar) + 1);
				if (cipherCharValue < 'a') {
					cipherCharValue = (cipherCharValue + LOWERCASE.length());
				}
				charArray[i] = (char) (cipherCharValue);
			}
		}
		return new String(charArray);
	}
}
