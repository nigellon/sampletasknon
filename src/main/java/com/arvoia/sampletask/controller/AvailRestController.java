package com.arvoia.sampletask.controller;

import com.arvoia.sampletask.model.AvailResponse;
import com.arvoia.sampletask.model.Error;
import com.arvoia.sampletask.model.Offer;
import com.arvoia.sampletask.service.AvailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import static com.arvoia.sampletask.constants.AvailResponseConstants.*;

@Configuration
@RestController
public class AvailRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AvailRestController.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private AvailService availService;

    @Value("${vendor.url}")
    private String vendorUrl;

    @GetMapping(value = "/avail", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody
    AvailResponse getAvailabilities() {
        AvailResponse ret = new AvailResponse();
        String searchResult = getSearchResults();

        LOGGER.info("Vendor Search Result: ", searchResult);

        Offer offer = null;
        Error error;
        if(searchResult != null) {
            offer = availService.getOffer(searchResult);
            error = availService.getError(searchResult);
//            if(error.getCode() > RESP_DEFAULT_ERROR_CODE) {
//                ret.setResult(RESP_ERROR);
//                ret.setError(error);
//            } else{
                ret.setOffer(offer);
                ret.setResult(RESP_SUCCESS);
//            }
        } else{
            // Service unavailable
            ret.setResult(RESP_ERROR);
            ret.setError(new Error());
        }

        return ret;
    }

    private String getSearchResults() {
        try {
            return restTemplate.getForEntity(vendorUrl, String.class).getBody();
        } catch (Exception e) {
            LOGGER.error("Error retrieving vendor vehicle availability", e.getMessage());
            return null;
        }
    }
}
