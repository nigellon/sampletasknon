package com.arvoia.sampletask.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class VehicleTest {

    @InjectMocks
    private Vehicle underTest;

    @Before
    public void setupTest() {
        // prepare test
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetBrand() {
        // given
        String value = "BMW";
        underTest.setBrand(value);
        // when
        String actual = underTest.getBrand();
        // then
        assertEquals(value, actual);
    }

    @Test
    public void testGetCategory() {
        // given
        String value = "Luxury";
        underTest.setCategory(value);
        // when
        String actual = underTest.getCategory();
        // then
        assertEquals(value, actual);
    }

    @Test
    public void testGetBasePrice() {
        // given
        int value = 620;
        underTest.setBasePrice(value);
        // when
        int actual = underTest.getBasePrice();
        // then
        assertEquals(value, actual);
    }

}
