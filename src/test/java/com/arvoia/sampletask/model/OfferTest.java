package com.arvoia.sampletask.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class OfferTest {

    @InjectMocks
    private Offer underTest;

    @Before
    public void setupTest() {
        // prepare test
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetTimeStamp() {
        // given
        Date value = new Date();
        underTest.setTimestamp(new Date());
        // when
        Date actual = underTest.getTimestamp();
        // then
        assertEquals(value, actual);
    }

    @Test
    public void testGetAvailability() {
        // given
        int value = 6;
        underTest.setAvailable(value);
        // when
        int actual = underTest.getAvailable();
        // then
        assertEquals(value, actual);
    }

    @Test
    public void testGetVehicles() {
        // given
        List<Vehicle> value = Mockito.mock(List.class);

        underTest.setVehicles(value);
        // when
        List<Vehicle> actual = underTest.getVehicles();
        // then
        assertEquals(value, actual);
    }
}
