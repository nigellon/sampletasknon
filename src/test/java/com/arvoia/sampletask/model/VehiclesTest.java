package com.arvoia.sampletask.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class VehiclesTest {

    @InjectMocks
    private Vehicles underTest;

    @Before
    public void setupTest() {
        // prepare test
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetVehicleList() {
        // given
        List<Vehicle> value = Mockito.mock(List.class);

        underTest.setVehicles(value);
        // when
        List<Vehicle> actual = underTest.getVehicleList();
        // then
        assertEquals(value, actual);
    }
}
