package com.arvoia.sampletask.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static com.arvoia.sampletask.constants.AvailResponseConstants.RESP_DEFAULT_ERROR_CODE;
import static com.arvoia.sampletask.constants.AvailResponseConstants.RESP_DEFAULT_ERROR_DESCRIP;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ErrorTest {

    @InjectMocks
    private Error underTest;

    @Before
    public void setupTest() {
        // prepare test
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetCode() {
        // given
        int value = RESP_DEFAULT_ERROR_CODE;
        underTest.setCode(value);
        // when
        int actual = underTest.getCode();
        // then
        assertEquals(value, actual);
    }

    @Test
    public void testGetDescription() {
        // given
        String value = RESP_DEFAULT_ERROR_DESCRIP;
        underTest.setDescription(value);
        // when
        String actual = underTest.getDescription();
        // then
        assertEquals(value, actual);
    }


}
