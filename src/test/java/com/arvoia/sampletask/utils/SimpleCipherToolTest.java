package com.arvoia.sampletask.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleCipherToolTest {
	
	private SimpleCipherTool sct = null;
	private final String KEY1 = "abc";
	private final String KEY2 = "arvoia";

	@Test
	public void cipher() { 
		sct = new SimpleCipherTool(KEY1);
    	assertEquals("bcd", sct.cipher("aaa"));
    	assertEquals("abc", sct.cipher("zzz"));
    	assertEquals("Scpqnh vdtm", sct.cipher("Sample task"));
	}
	
	@Test
	public void decipher() { 
		sct = new SimpleCipherTool(KEY1);
    	assertEquals("aaa", sct.decipher("bcd"));
    	assertEquals("zzz", sct.decipher("abc"));
    	assertEquals("Sample task", sct.decipher("Scpqnh vdtm"));
    	assertEquals("Sample task", sct.decipher("Scpqnh vdtm"));
	}
	
	@Test
	public void decipher_uri() { 
		sct = new SimpleCipherTool(KEY2);
    	assertEquals("https://bitbucket.org/arvoiatechtests/sampletasknon", 
    			sct.decipher("ilpeb://xxccvugtc.pjc/jswgepcfdzptbut/opvqmwppblogj"));
	}
}
